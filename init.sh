#!/bin/bash

## Prepare directory structure
## -----------------------------------------------------------------------------

CONF_DIR="/ark/config"
DATA_DIR="/ark/server"
SAVE_DIR=${DATA_DIR}/ShooterGame/Saved

[ ! -d ${DATA_DIR}/backup ] && mkdir -p ${DATA_DIR}/backup
[ ! -d ${DATA_DIR}/cluster ] && mkdir -p ${DATA_DIR}/cluster
[ ! -d /ark/logs ] && mkdir -p /ark/logs
[ ! -d ${DATA_DIR}/ShooterGame ] && mkdir -p ${SAVE_DIR}/Config/LinuxServer

rm -f ${SAVE_DIR}/Config/LinuxServer/Game.ini
ln -s ${CONF_DIR}/Game.ini ${SAVE_DIR}/Config/LinuxServer/Game.ini

rm -f ${SAVE_DIR}/Config/LinuxServer/GameUserSettings.ini
ln -s ${CONF_DIR}/GameUserSettings.ini ${SAVE_DIR}/Config/LinuxServer/GameUserSettings.ini

## Install / update ark
## -----------------------------------------------------------------------------

[ ! -f ${DATA_DIR}/version.txt ] && arkmanager install --verbose

#arkmanager installmods --verbose
arkmanager update --verbose
