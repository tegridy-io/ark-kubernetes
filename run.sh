#!/bin/bash

## Create pipe for input
## -----------------------------------------------------------------------------

[ -p /tmp/FIFO ] && rm /tmp/FIFO
mkfifo /tmp/FIFO

## Start server
## -----------------------------------------------------------------------------

arkmanager start --noautoupdate --verbose

## Stop server in case of signal INT or TERM
## -----------------------------------------------------------------------------

function stop {
  arkmanager backup --verbose
  arkmanager stop --warn
}

echo "Waiting..."
trap stop INT
trap stop TERM

## Read from input and wait indefinetly
## -----------------------------------------------------------------------------

read < /tmp/FIFO &
wait
