# ARK: Survival Evolved - Kubernetes Image

Docker build for managing an ARK: Survival Evolved cluster.

Use like this: `registry.gitlab.com/tegridy-io/ark-kubernetes/ark-kubernetes:{TAG}`

Available tags: https://gitlab.com/tegridy-io/ark-kubernetes/container_registry/3513547

Helm chart: https://gitlab.com/tegridy-io/charts/-/tree/main/charts/ark-cluster
