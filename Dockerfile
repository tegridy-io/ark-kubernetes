## Dependencies
## -----------------------------------------------------------------------------
FROM ubuntu:jammy AS base

RUN apt-get update && apt-get upgrade -y \
    && apt-get install -y curl bzip2 perl-modules lsof libc6-i386 \
    # cleanup
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/* \
    && rm -rf /var/tmp/*


## Install steamcmd
## -----------------------------------------------------------------------------
FROM base AS steamcmd

ARG UBUNTU_VERSION=latest
# i386 is required until the day `steamcmd` is built for amd64 and also games no longer need i386 libs.
RUN dpkg --add-architecture i386 \
    && apt-get update && apt-get upgrade -y \
    && echo steam steam/question select "I AGREE" | debconf-set-selections \
    && echo steam steam/license note '' | debconf-set-selections \
    && apt-get install -y ca-certificates steamcmd locales \
    && locale-gen en_US.UTF-8 \
    # cleanup
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/* \
    && rm -rf /var/tmp/*

RUN ln -s /usr/games/steamcmd /usr/local/bin

# install steamcmd and remove downloaded packages to reduce image size
RUN steamcmd +quit \
    && rm -rf /home/steam/.local/share/Steam/steamcmd/package/*.zip.*


## Install ark
## -----------------------------------------------------------------------------
FROM steamcmd

# add user steam and put in root group
ARG UID=1000 \
    GID=0

RUN adduser --gecos "" --disabled-password --gid 0 steam

# install arkmanager
ARG AMG_VERSION=v1.6.62

RUN curl -sL "https://raw.githubusercontent.com/arkmanager/ark-server-tools/$AMG_VERSION/netinstall.sh" | bash -s steam
RUN ln -s /usr/local/bin/arkmanager /usr/bin/arkmanager

# prepare directory layout
RUN rm -r /etc/arkmanager \
    && mkdir -p /ark/config \
    && mkdir -p /ark/logs \
    && mkdir -p /ark/server/backup \
    && mkdir -p /ark/server/cluster

# copy files
COPY arkmanager/ /etc/arkmanager/
COPY init.sh /init.sh
COPY run.sh /run.sh

# change permissions so kubernetes user has access (group=0)
RUN chgrp -R 0 /home/steam /etc/arkmanager /ark/config /ark/server /init.sh /run.sh \
    && chmod -R g=u /home/steam /etc/arkmanager /ark/config /ark/server /init.sh /run.sh

# VOLUME /ark/backup /ark/logs /ark/cluster /ark/server /etc/arkmanager
VOLUME /ark/config /ark/logs /ark/server /etc/arkmanager

# setup environment
ENV PORT_GAME="7778" \
    PORT_QUERY="27016" \
    ENABLE_RCON="true" \
    PORT_RCON="27017" \
    CLUSTER_NAME="ark-cluster" \
    GAME_INSTANCE="main" \
    GAME_MAP="Fjordur" \
    GAME_SESSION="Welcome to Fjordur" \
    GAME_SERVERPW="" \
    GAME_ADMINPW="SECRET_PW" \
    GAME_MAXPLAYERS="10" \
    MOD_IDS=""

WORKDIR /ark

USER $UID

CMD [ "/init.sh" ]
